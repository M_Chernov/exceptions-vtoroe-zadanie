package com.company;

public class StringTwo {
    //1.Return the number of times that the string "hi" appears anywhere in the given string.

    /*2.Return the number of times that the string "code" appears anywhere in the given string,
    except we'll accept any letter for the 'd', so "cope" and "cooe" count.
    */

    /*
    Given two strings, return true if either of the strings appears at the very end of the other string,
    ignoring upper/lower case differences (in other words, the computation should not be "case sensitive").
    Note: str.toLowerCase() returns the lowercase version of a string.
     */

    public int countHi(String str) {
        int a = 0;
        char[] chars = str.toCharArray();
        for (int i = 0; i < str.length() - 1; i++) {
            if (chars[i] == 'h' && chars[i + 1] == 'i') {
                a++;
            }
        }
        return a;
    }

    public int countCode(String str) {
        int a = 0;
        char[] chars = str.toCharArray();
        for (int i = 0; i < str.length() - 3; i++) {
            if (chars[i] == 'c' && chars[i + 1] == 'o' && chars[i + 3] == 'e') {
                a++;
            }
        }
        return a;
    }

    public boolean endOther(String a, String b) {
        String aLower = a.toLowerCase();
        String bLower = b.toLowerCase();
        int aLength = a.length();
        int bLength = b.length();
        if (a.length() > b.length()) {
            return aLower.substring(aLength - bLength).contains(bLower);
        } else return bLower.substring(bLength - aLength).contains(aLower);
    }
}
